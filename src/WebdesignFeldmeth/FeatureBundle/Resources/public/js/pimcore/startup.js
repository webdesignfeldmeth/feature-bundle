pimcore.registerNS("pimcore.plugin.FeatureBundle");

pimcore.plugin.FeatureBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.FeatureBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("FeatureBundle ready!");
    }
});

var FeatureBundlePlugin = new pimcore.plugin.FeatureBundle();
