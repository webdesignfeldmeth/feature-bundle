<?php

namespace WebdesignFeldmeth\FeatureBundle\Model;

use Symfony\Component\Yaml\Yaml;

class Feature {

	public function __construct() {
	}

	/**
	 * Gibt den Bundle Namen des Projekte zurück, damit feature.yml gefunden wird
	 */
	public static function getProjectBundle($path) {
		$path = explode("/", $path);
		// Controller entfernen
		array_pop($path);
		// Element Position vor dem Bundlenamen
		$positionStartBundle = array_search("src", $path) + 1;
		$positionEndBundle = sizeof($path) - 1;
		$bundle = [];
		for($i = $positionStartBundle; $i <= $positionEndBundle; $i++) {
			$bundle[] = str_replace("Bundle", "", $path[$i]);
		}
		return implode("/", $bundle);
	}

	public static function setStyle($feature, $path) {
		$file = $path . "feature.yml";
		$file = realpath($file);
		$values = Yaml::parse(file_get_contents($file));
		$styles = $values["feature"][$feature]["files"];
		$count = 0;
		foreach($styles as $style) {
			if($style["type"] == "css") {
				$_SESSION["style"][$feature][$count] = $style["file"];
				$count++;
			}
		}
	}

	public static function getStyles() {
		$stylesToLoad = "";

		if($_SESSION["style"]) {
			foreach($_SESSION["style"] as $styles) {
				foreach($styles as $style) {
					$stylesToLoad .= "<link rel='stylesheet' type='text/css' href='" . $style. "'>";
				}
			}
		}

		return $stylesToLoad;

	}

	public static function setScript($feature, $path) {
		$file = $path . "feature.yml";
		$file = realpath($file);
		$values = Yaml::parse(file_get_contents($file));

		$scripts = $values["feature"][$feature]["files"];

		$count = 0;
		foreach($scripts as $script) {
			if($script["type"] == "js") {
				$_SESSION["script"][$feature][$count] = $script["file"];
				$count++;
			}
		}
	}

	public static function getScripts() {
		$scriptsToLoad = "";

		if($_SESSION["script"]) {
			foreach($_SESSION["script"] as $scripts) {
				foreach($scripts as $script) {
					$scriptsToLoad .= "<script src='" . $script. "'></script>";
				}
			}
		}

		return $scriptsToLoad;
	}
}