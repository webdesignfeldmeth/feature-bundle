<?php

namespace WebdesignFeldmeth\FeatureBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class FeatureBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/feature/js/pimcore/startup.js'
        ];
    }
}