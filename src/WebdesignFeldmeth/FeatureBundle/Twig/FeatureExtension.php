<?php

namespace WebdesignFeldmeth\FeatureBundle\Twig;

use Aufwind\Stdlib\Html\Node;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Pimcore\Model\DataObject;
use WebdesignFeldmeth\FeatureBundle\Model\Feature;

/**
 * LoaderExtension
 *
 * LICENSE: Copyright (c) 2020, Webdesign Feldmeth
 *
 * @category  WebdesignFeldmeth\DesignBundle
 *
 * @copyright 2020 Webdesign Feldmeth
 * @license   keine Lizenz angegeben
 *
 * @version   1.0.0
 *
 * @see       https://bitbucket.org/aufwindteam/aufwinddefault
 */
class FeatureExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('set_design_style', function ($feature = "", $path = "") {
                $path = $path == "" ? dirname(__FILE__) . "/../Resources/config/pimcore/" : $path;
                Feature::setStyle($feature, $path);
            }),

            new TwigFunction('get_design_styles', function() {
                return Feature::getStyles();
            }),

            new TwigFunction('set_design_script', function ($feature = "", $path = "") {
                $path = $path == "" ? dirname(__FILE__) . "/../Resources/config/pimcore/" : $path;
                Feature::setScript($feature, $path);
            }),

            new TwigFunction('get_design_scripts', function () {
                return Feature::getScripts();
            })

        ];
    }

}
